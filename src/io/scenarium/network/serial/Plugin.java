package io.scenarium.network.serial;

import io.beanmanager.PluginsBeanSupplier;
import io.beanmanager.consumer.ClassNameRedirectionConsumer;
import io.scenarium.flow.consumer.OperatorConsumer;
import io.scenarium.flow.consumer.PluginsFlowSupplier;
import io.scenarium.network.serial.operator.network.bus.serial.RXTXSerial;
import io.scenarium.pluginManager.PluginsSupplier;

public class Plugin implements PluginsSupplier, PluginsFlowSupplier, PluginsBeanSupplier {
	@Override
	public void populateOperators(OperatorConsumer operatorConsumer) {
		operatorConsumer.accept(RXTXSerial.class);
	}

	@Override
	public void populateClassNameRedirection(ClassNameRedirectionConsumer classNameRedirectionConsumer) {
		classNameRedirectionConsumer.accept("io.scenarium.core.operator.network.bus.serial.RXTXSerial", "io.scenarium.network.serial.operator.network.serial.RXTXSerial");
	}

}
