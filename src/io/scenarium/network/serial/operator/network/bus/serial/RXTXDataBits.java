/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.network.serial.operator.network.bus.serial;

import gnu.io.SerialPort;

public enum RXTXDataBits {
	DATABITS_5(SerialPort.DATABITS_5), DATABITS_6(SerialPort.DATABITS_6), DATABITS_7(SerialPort.DATABITS_7), DATABITS_8(SerialPort.DATABITS_8);

	private final int value;

	private RXTXDataBits(int value) {
		this.value = value;
	}

	public int getValue() {
		return this.value;
	}
}
