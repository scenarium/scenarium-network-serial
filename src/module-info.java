import io.scenarium.network.serial.Plugin;
import io.scenarium.pluginManager.PluginsSupplier;

module io.scenarium.network.serial {
	uses PluginsSupplier;

	provides PluginsSupplier with Plugin;

	requires transitive io.scenarium.flow;

	requires transitive JRxTx;

	exports io.scenarium.network.serial;
	exports io.scenarium.network.serial.operator.network.bus.serial;

}
